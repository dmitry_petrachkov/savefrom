# Savefrom
Моя реализация сайта https://ru.savefrom.net/, который позволяет скачивать видео и аудио с различных веб-сайтов, например с YouTube, Twitter, ВКонтакте и т.д.

Демонстрация: https://www.youtube.com/watch?v=LsVeefe2MXs

Возможности моей реализации:
- Загрузка видео со 1161 сайта, включая YouTube, Instagram, ВКонтакте, Twitter, Faceobook
- Выбор качества видео и звука
- Скачивание только звуковой дорожки

Использованные инструменты и технологии:
- Git
- HTML5, CSS3, JavaScript
- Bootstrap
- Node.js

Установка:
- Отредактировать config/default.json
- Запустить!