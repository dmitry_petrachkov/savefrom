const express = require('express');
const config = require('config');
const logger = require('./logger');
const path = require('path');
const fs = require('fs');
const uniqueFilename = require('unique-filename');
const childProcess = require('child_process');
const tableParser = require('table-parser');

const app = express();

function errorHandle(err, res) {
    logger.error(`(${err.name}) ${err.message}\n${err.stack}`);
    res.sendStatus(404);
}

function uniqueDirectory(callback) {
    const dir = uniqueFilename(config.get('server.content_directory'));
    fs.mkdir(dir, (err) => {
        callback(err, dir);
    });
}

// Получение заголовка видео
app.get('/title', (req, res) => {
    childProcess.execFile(
        config.get('youtube-dl.path'), 
        ['--get-title', '--encoding', 'utf-8', req.query.url],
        (err, stdout) => {
            if(err) errorHandle(err, res) 
            else {
                res.send(stdout);
            } 
        } 
    )
         
});

// Получение превью видео
app.get('/thumbnail', (req, res) => {
    uniqueDirectory((err, dir) => {
        if(err) errorHandle(err, res) 
        else {
            childProcess.execFile(
                config.get('youtube-dl.path'), 
                ['--write-thumbnail', '--skip-download', req.query.url], 
                { cwd: dir},
                (err, stdout) => {
                    if(err) errorHandle(err, res) 
                    else {
                        // Поиск загруженного превью
                        fs.readdir(dir, (err, files) => {
                            if(err) errorHandle(err, res) 
                            else {
                                let thumbname = files[0];
                                res.sendFile(path.join(dir, thumbname));
                            }
                        });
                    }
                }
            );
        }
    })
});

// Получение списка доступных форматов
app.get('/formats', (req, res) => {
    let output = childProcess.execFile(
        config.get('youtube-dl.path'), 
        ['-F', req.query.url],
        (err, stdout) => {
            if(err) errorHandle(err, res) 
            else {
                let output = stdout.toString();
                output = output.substr(output.indexOf('format code'));  
                output = output.replace('format code', 'format_code');
            
                let table = tableParser.parse(output);
                res.send(table);
            }
        }
    );    
});

// Получение видео-файла
app.get('/download/:filename', (req, res) => {
    let ydl = childProcess.spawn(
        config.get('youtube-dl.path'), 
        ['--quiet', '-f', req.query.code, req.query.url, '-o', '-']
    );

    res.writeHead(200, {
        'Access-Control-Allow-Origin': '*',
        'Connection': 'Keep-Alive',
        'Content-Type': 'application/octet-stream'
    });

    ydl.stdout.on('data', (chunk) => {
        res.write(chunk);
    });

    ydl.on('error', (err) => {
        errorHandle(err, res);
    });
});

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});

app.listen(config.get('server.port'), () => {
    logger.info(`Server started at http://localhost:${config.get('server.port')}`);
});